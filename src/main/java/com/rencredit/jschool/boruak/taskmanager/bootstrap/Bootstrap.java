package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;
import com.rencredit.jschool.boruak.taskmanager.controller.CommandController;
import com.rencredit.jschool.boruak.taskmanager.repository.CommandRepository;
import com.rencredit.jschool.boruak.taskmanager.service.CommandService;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final CommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        parseArgs(args);
        while (true) {
            processCommand();
        }
    }

    public void processCommand() {
        final Scanner scanner = new Scanner(System.in);
        String[] commands;
        while (true) {
            System.out.print("Enter command: ");
            commands = scanner.nextLine().split("\\s+");
            System.out.println();
            parseArgs(commands);
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        for (String arg : args) {
            processArg(TerminalCommandUtil.convertArgumentToCommand(arg));
            System.out.println();
        }
    }

    private void processArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                System.exit((0));
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showUnknownCommand(arg);
        }
    }

}
