package com.rencredit.jschool.boruak.taskmanager.api;

import com.rencredit.jschool.boruak.taskmanager.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
