package com.rencredit.jschool.boruak.taskmanager.api;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

    void showUnknownCommand(final String arg);

    void showInfo();

}
